from django.shortcuts import render
from django.http import HttpResponse

def index(request):
	return render (request,'index.html')

def about(request):
	return render (request, 'About Me.html')

def contact(request):
	return render (request, 'Contact.html')

def gallery(request):
	return render (request, "Galery.html")

