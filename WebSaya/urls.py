from django.contrib import admin
from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name = 'index'),
   	path('AboutMe/', views.about, name = 'about'),
   	path('Contact/', views.contact, name ='contact'),
   	path('Gallery/', views.gallery, name ='gallery'),

]